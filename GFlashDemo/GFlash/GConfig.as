﻿package GFlash
{
    import flash.xml.*;
	import flash.net.*;
	import flash.events.*;
	
	public class GConfig
	{
		
		private var gFilePath:String="";
		private var gIsLoaded:Boolean=false;
		
		private var gFileTypeArr:Array=["xml","ini"];
		private var gType:String="";
		
		private var gURLRequest:URLRequest;
		private var gURLLoader:URLLoader;
		
		private var gXML:XML;
		private var gCallBackFun:Function;
		
		public function GConfig(path:String,callback:Function):void 
		{
			if(path.length==0)
			{
				trace("Error:Invalid Path!!!");
				return;
			}
			var TypeErr:Boolean=true;
			for(var i:int=0;i<gFileTypeArr.length;i++)
			{
				gType=path.substr( path.lastIndexOf(".")+1,path.length).toLowerCase();
				if(gType==gFileTypeArr[i])
				{
					TypeErr=false;
				}
			}
			if(TypeErr)
			{
				trace("Error:Invalid File Type!!!");
				return;
			}
			if(!callback)
			{
				trace("Error:CallBack Function Is NULL!!!");
				return;
			}
			gFilePath=path;
			gCallBackFun=callback;
			if(gType=="xml")
			{
				gURLRequest= new URLRequest(path);
				gURLLoader= new URLLoader();
				gURLLoader.addEventListener(Event.COMPLETE, OnLoadXMLComplete);
				gURLLoader.addEventListener(IOErrorEvent.IO_ERROR, OnXMLErrorHandler);
                gURLLoader.load(gURLRequest);
			}else if(gType=="ini")
			{
				
			}
		}
		private function OnLoadXMLComplete(e:Event):void
		{
			trace("--------------------Data---------------------");
			trace(e.target.data);
			trace("---------------------------------------------");
			gXML = XML(e.target.data);
			gIsLoaded=true;
			gCallBackFun();
		}
		private function OnXMLErrorHandler(e:IOErrorEvent):void
		{
			trace("Error:Load ["+gFilePath+"] Failed!!!!!!");
			gIsLoaded=false;
		}
		public  function GetParam(key:String):String 
		{
			var param:String="";
			if(gIsLoaded)
			{
				param=gXML.child(key);
			}
			return param;
		}
		
	}
}
 