﻿package GFlash
{
	import flash.display.Sprite;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.Event;
	import flash.errors.IOError;
    import flash.events.IOErrorEvent;

	public class GSound extends Sprite
	{
		private var gSound:Sound;
		private var gRequest:URLRequest;
		private var gChannel:SoundChannel = new SoundChannel();
		
		private var gSoundUrl:String = "";
		private var gLoop:Boolean = false;
		private var gAutoPlay:Boolean = true;
		private var gCurrentTime:Number = 0.0;
		private var gStopTime:Number = 0.0;
		private var gIsPlaying:Boolean = false;
		//
		private var gSupportTypeArr:Array=["mp3","wav"];
		private var gType:String="";
		public function GSound(path:String,autoStart:Boolean=true,loop:Boolean=false)
		{
			if(path.length==0)
			{
				trace("Error:Invalid Path!!!");
				return;
			}
			var TypeErr:Boolean=true;
			for(var i:int=0;i<gSupportTypeArr.length;i++)
			{
				gType=path.substr( path.lastIndexOf(".")+1,path.length).toLowerCase();
				if(gType==gSupportTypeArr[i])
				{
					TypeErr=false;
				}
			}
			if(TypeErr)
			{
				trace("Error:Invalid Music Type!!!");
				return;
			}
			if(gType=="wav")
			{
				trace("Warnning:no voice for wav");
			}
			gLoop = loop;
			gSoundUrl = path;
			gAutoPlay = autoStart;
			gRequest = new URLRequest(path);
			gSound= new Sound(gRequest);
			gSound.addEventListener(IOErrorEvent.IO_ERROR,OnErrorHandler);
			gSound.addEventListener(Event.COMPLETE,OnLoadCompleted);

		}
		private function OnErrorHandler(e:Event)
		{
			trace("Error:Load ["+gSoundUrl+"] Failed!!!!");
			gSound.removeEventListener(IOErrorEvent.IO_ERROR,OnErrorHandler);
			gSound.removeEventListener(Event.COMPLETE,OnLoadCompleted);
		}
		private function OnLoadCompleted(e:Event)
		{
			ShowInfo();
			if (gAutoPlay)
			{
				gChannel = gSound.play();
				gIsPlaying = true;
			}
			else
			{
				gChannel = gSound.play();
				gChannel.stop();
				gIsPlaying = false;
			}
			this.addEventListener(Event.ENTER_FRAME, OnUpdate);
		}
		private function ShowInfo()
		{
			//
			
			////信息输出
			trace("----------["+gSoundUrl+"]----------");
			trace("Time:"+gSound.length+"(ms)");
			trace("Type:"+gType);
			trace("IsAutoPlay:"+gAutoPlay);
			trace("IsLoop:"+gLoop);
			trace("-----------------------------------");
		}
		private function OnUpdate(e:Event)
		{
			if (gIsPlaying)
			{
				if (gChannel.position >= gSound.length - 1000)
				{
					//留一些时间处理
					if (gLoop)
					{
						trace("loop......");
						gChannel=gSound.play(0.0);
						gStopTime = 0.0;
						gIsPlaying = true;
					}
					else
					{
						this.removeEventListener(Event.ENTER_FRAME, OnUpdate);
						//gChannel.stop();
						//gSound.close();
						gStopTime = 0.0;
						gIsPlaying = false;
					}
				}
			}
		}
		public function Play(startTime:Number=0.0)
		{
			gChannel=gSound.play(gStopTime);
			gIsPlaying = true;
		}
		public function Pause()
		{
			gStopTime = gChannel.position;
			gChannel.stop();
			gIsPlaying = false;
		}
		public function Stop()
		{
			gChannel.stop();
			gSound.close();
			gStopTime = 0.0;
			gIsPlaying = false;
		}
		public function IsSoundPlaying()
		{
			return gIsPlaying;
		}
		public function GetTotalTime()
		{
			return gSound.length;
		}
		public function GetCurrentTime()
		{
			return gChannel.position;
		}
		//...........class end
	}
}