﻿
package GFlash
{
	import flash.display.*;
    import flash.events.*;
    import flash.net.*;
	
	public class GLoader 
	{
		private static var gFunction:Function;
		private static var gLoader:Loader;
		private static var gImgData:BitmapData;
		private static var gTextData:String;
		
		////////////////////////////////////----image----////////////////////////////////////////////
		public static function LoadImage_Begin(path:String,callBack:Function): void
		{
			trace("Load Image");
			if(gLoader)
			{
				//gLoader.unload();
				//gLoader.close();
			}
			gLoader=new Loader()
			gLoader.contentLoaderInfo.addEventListener(Event.COMPLETE,OnImgLoadComplete);
            gLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR,OnImgLoadError);
            gLoader.load(new URLRequest(path));
			gFunction=callBack;
		}
		public static function  LoadImage_End():BitmapData
		{
			trace("end");
			return gImgData;
		}
		private static function OnImgLoadComplete(e:Event):void
	    {
			gImgData = new BitmapData(gLoader.width, gLoader.height, true, 0xFFFFFFFF); 
			gImgData.draw(gLoader);
			gFunction();
		}
		private static function OnImgLoadError(e:IOErrorEvent):void
		{
			trace("Error");
		}
		
		///////////////////////////////////////----text----//////////////////////////////////////////////
		public static function LoadText_Begin(path:String,callBack:Function): void
		{
			trace("Load Text:"+path);
			
			var gURLLoder:URLLoader=new URLLoader();
			gURLLoder.addEventListener(Event.COMPLETE,OnTextLoadComplete);
            gURLLoder.addEventListener(IOErrorEvent.IO_ERROR,OnTextLoadError);
            gURLLoder.load(new URLRequest(path));
			
			gFunction=callBack;
		}
		
		public static function  LoadText_End():String
		{
			trace("end");
			return gTextData;
		}
		private static function OnTextLoadComplete(e:Event):void
	    {
			gTextData=String(e.target.data); 
			gFunction();
		}
		private static function OnTextLoadError(e:IOErrorEvent):void
		{
			trace("Error"+e.text);
		}
		
		
		
		
	}
}
 