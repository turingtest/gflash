﻿
package GFlash
{
	import flash.display.*;
    import flash.text.*;
	import flash.events.*;
	import flash.filters.DropShadowFilter;
	
	public class GMessageBox 
	{
		private static var gMainStage:Stage;
		private static var gTipsTitle:String="Tips";
		private static var gBoxWidth:int=200;
		private static var gBoxHeight:int=100;
		private static var gIsCreatedWnd:Boolean=false;
		private static var gBkBmpData:BitmapData=new BitmapData(gBoxWidth,gBoxHeight,true,0xAF606060);
		private static var gBoxWindow:Sprite = new Sprite();

	
		public static function InitStage(stageReference:Stage):void 
		{
			gMainStage = stageReference;
			if(gMainStage)
			{
				gBoxWindow.graphics.lineStyle(3,0x0099ff);
			    gBoxWindow.graphics.beginFill(0xffffff);
			    gBoxWindow.graphics.drawRoundRect(0,0,gBoxWidth,gBoxHeight,5);
				gBoxWindow.graphics.beginBitmapFill(gBkBmpData);
                gBoxWindow.graphics.drawRect(0,0,gBoxWidth,gBoxHeight);
			    gBoxWindow.graphics.endFill();
			    gBoxWindow.alpha=1;
				gBoxWindow.x=gMainStage.stageWidth/2-gBoxWidth/2;
				gBoxWindow.y=gMainStage.stageHeight/2-gBoxHeight/2;
	            gMainStage.addChild(gBoxWindow);
				if(!gIsCreatedWnd)
				{
					CreateWindow();
					gBoxWindow.visible=false;
					gBoxWindow.addEventListener(MouseEvent.MOUSE_DOWN,OnStartDrag);
					gBoxWindow.addEventListener(MouseEvent.MOUSE_UP,OnStopDrag);
				}
			}
		}
		public static function RemoveWindow():void 
		{
			gBkBmpData.dispose();
			gMainStage.removeChild(gBoxWindow);
			gBoxWindow.removeEventListener(MouseEvent.MOUSE_DOWN,OnStartDrag);
			gBoxWindow.removeEventListener(MouseEvent.MOUSE_UP,OnStopDrag);
		}
		public static function SetTitle(text:String):void 
		{
			for(var i:uint=0;i<text.length;i++)
			{
				//大于255则是汉字，汉字占两个字符
				if(text.charCodeAt(i)>255)
				{
					return;
				}
			}
		    gTipsTitle=text;
			var TitleText:TextField=TextField(gBoxWindow.getChildByName("gTitle"));
			TitleText.text=gTipsTitle;
		}

		public static function show(info:String):void 
		{
			if (gMainStage == null) return;
			if(gIsCreatedWnd&&!gBoxWindow.visible)
			{
				gBoxWindow.visible=true;
				gBoxWindow.getChildByName("OK").addEventListener(MouseEvent.CLICK,OnOk);
				var gBodyText:TextField=TextField(gBoxWindow.getChildByName("gBody"));
				gBodyText.text=info;
			}
			
		}
		private static function CreateWindow():void
		{
			//title
			var gTitleField:TextField = new TextField();
			gTitleField.textColor = 0xFFFFFFFF;
			gTitleField.name = "gTitle";
			gTitleField.text=gTipsTitle;
			gTitleField.selectable = false;
			gTitleField.autoSize = TextFieldAutoSize.LEFT;
			gBoxWindow.addChild(gTitleField);
			//line
			var gLine:Sprite=new Sprite();
			gLine.graphics.lineStyle(2,0x0099ff);
		    gLine.graphics.moveTo(0,20);
			gLine.graphics.lineTo(gBoxWidth,20);
			gBoxWindow.addChild(gLine);
			//body
			var gBodyField:TextField = new TextField();
			gBodyField.textColor = 0xFFFFFFFF;
			gBodyField.name = "gBody";
			gBodyField.text="测试测试,这是一条简单的消息！";
			gBodyField.multiline = true;
			gBodyField.selectable = false;
			gBodyField.autoSize = TextFieldAutoSize.CENTER;
			gBodyField.x=15;
			gBodyField.y=35;
			gBoxWindow.addChild(gBodyField);
			//btn
			var gBtnOk:Sprite=new Sprite();
			gBtnOk.name="OK";
			gBtnOk.graphics.lineStyle(2,0x0099ff);
			gBtnOk.graphics.beginFill(0xffffff);
			gBtnOk.graphics.drawRoundRect(0,0,40,20,5);
			gBtnOk.graphics.endFill();
			gBtnOk.x=gBoxWidth/2-20;
			gBtnOk.y=gBoxHeight-30;
			
			var gBtnOkText:TextField = new TextField();
			gBtnOkText.textColor = 0x00000000;
			gBtnOkText.text="OK";
			gBtnOkText.selectable = false;
			gBtnOkText.autoSize = TextFieldAutoSize.CENTER;
			gBtnOkText.x=8;
			gBtnOkText.y=0;
			gBtnOk.addChild(gBtnOkText);
			gBoxWindow.addChild(gBtnOk);
			gBoxWindow.getChildByName("OK").addEventListener(MouseEvent.CLICK,OnOk);
			gIsCreatedWnd=true;
		}
		private static function OnOk(e:MouseEvent):void 
		{
			//trace("OK");
			gBoxWindow.visible=false;
			gBoxWindow.getChildByName("OK").removeEventListener(MouseEvent.CLICK,OnOk);
		}
		private static function OnStartDrag(e:MouseEvent)
        {
			if(gBoxWindow.visible)
			{
				 gBoxWindow.startDrag();
			}
        }
		private static function OnStopDrag(e:MouseEvent)
        {
			if(gBoxWindow.visible)
			{
	            gBoxWindow.stopDrag();
			}
        }
	}
}
 