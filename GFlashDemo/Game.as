﻿package  {
	
	import flash.display.MovieClip;
	import GFlash.GMessageBox;
	import GFlash.GConfig;
	
	public class Game extends MovieClip {
		
		var xml:GConfig;
		public function Game() 
		{
			// constructor code
			GMessageBox.InitStage(stage);
			xml=new GConfig("config.xml",Load);
		}
		public function Load()
		{
			var IP=xml.GetParam("BKMusicPath");
			GMessageBox.show(IP);
		}
	}
	
}
